################# KUBECTL #################
# Install on Windows
iwr -useb get.scoop.sh | iex
scoop install kubectl

# Install on Linux
sudo snap install kubectl --classic


# Create dev namespace 
kubectl create namespace dev

# Check namespace is created
kubectl get namespaces

# Create users and hotels deployments
kubectl apply -f users.yaml -n dev
kubectl apply -f hotels.yaml -n dev

# Show Pods in dev namespace
kubectl get pods -n dev 

# Show services in dev namespace
kubectl get svc -n dev
