################# KUBECTL #################
# Install on Windows
iwr -useb get.scoop.sh | iex
scoop install kubectl

# Linux
sudo snap install kubectl --classic

# Port Forward
kubectl port-forward service/users-svc -n dev 3001:3000
kubectl port-forward service/hotels-svc -n dev 3002:3000

################# KUBEFWD #################
# install on Windows
iwr -useb get.scoop.sh | iex
scoop install kubefwd

# Install on Linux
wget https://github.com/txn2/kubefwd/releases/download/1.21.0/kubefwd_Linux_x86_64.tar.gz
tar -xf kubefwd_Linux_x86_64.tar.gz kubefwd
sudo mv kubefwd /usr/local/bin/
sudo chmod +x /usr/local/bin/kubefwd

# Port Forward 
sudo -E kubefwd svc -n dev
